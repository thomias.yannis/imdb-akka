# imdb-akka

## Getting Started

It's a simple POC. Please run `MovieServiceImplSpec` as ScalaTest to execute it.
In the second test, you can change the `title` value to the movie's original title
that you want.
