package com.gitlab.chibitomo

import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.alpakka.csv.scaladsl.CsvToMap
import akka.stream.scaladsl.Compression
import akka.stream.scaladsl.FileIO
import akka.stream.scaladsl.Source
import akka.util.ByteString
import MovieService.Episode
import MovieService.PrincipalId
import MovieService.PrincipalTmp
import MovieService.Title
import MovieService.TitleId
import MovieService.TitlePrincipal
import com.gitlab.chibitomo.MovieService.{Episode, PrincipalTmp, Title, TitlePrincipal}
import io.scalaland.chimney.Transformer
import io.scalaland.chimney.dsl.TransformerOps

import java.io.File

object DataSource {
  def openTsv[T](filename: String)(implicit transformer: Transformer[Map[String, String], T]): Source[T, _] =
    FileIO
      .fromPath(new File(getClass.getResource(filename).getFile).toPath)
      .via(Compression.gunzip())
      .map(adaptRaw)
      .via(CsvParsing.lineScanner(delimiter = CsvParsing.Tab))
      .via(CsvToMap.toMapAsStrings())
      .map(_.into[T].transform)

  private def adaptRaw[T](bs: ByteString) = ByteString(
    bs.utf8String
      .replaceAll("\\\\\"", "")
      .replaceAll("\"", "")
  )

  def getValue(map: Map[String, String], key: String, default: String = ""): String = map.get(key) match {
    case None        => default
    case Some("\\N") => default
    case Some(value) => value
  }

  def getMaybeValue(map: Map[String, String], key: String): Option[String] = map.get(key) match {
    case None        => None
    case Some("\\N") => None
    case someValue   => someValue
  }

  implicit val titleTransformer: Transformer[Map[String, String], Title] = (src: Map[String, String]) =>
    Title(
      TitleId(src("tconst")),
      src("originalTitle"),
      DataSource.getValue(src, "startYear", "-1").toInt,
      DataSource.getMaybeValue(src, "endYear").map(_.toInt),
      DataSource.getMaybeValue(src, "genres") match {
        case None      => List.empty[String]
        case Some(str) => str.split(',').toList
      }
    )
  implicit val titlePrincipalTransformer: Transformer[Map[String, String], TitlePrincipal] =
    (src: Map[String, String]) => TitlePrincipal(TitleId(src("tconst")), PrincipalId(src("nconst")))
  implicit val episodeTransformer: Transformer[Map[String, String], Episode] =
    (src: Map[String, String]) => Episode(TitleId(src("tconst")), TitleId(src("parentTconst")))
  implicit val principalTransformer: Transformer[Map[String, String], PrincipalTmp] = (src: Map[String, String]) =>
    PrincipalTmp(
      PrincipalId(src("nconst")),
      src("primaryName"),
      DataSource.getValue(src, "birthYear", "-1").toInt,
      DataSource.getMaybeValue(src, "deathYear").map(_.toInt),
      DataSource.getMaybeValue(src, "primaryProfession") match {
        case None      => List.empty[String]
        case Some(str) => str.split(',').toList
      }
    )

}
