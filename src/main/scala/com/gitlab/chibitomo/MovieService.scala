package com.gitlab.chibitomo

import akka.stream.scaladsl.Source
import MovieService.Episode
import MovieService.Principal
import MovieService.PrincipalTmp
import MovieService.Title
import MovieService.TitleId
import MovieService.TitlePrincipal
import MovieService.TvSeries
import io.scalaland.chimney.dsl.TransformerOps

// DO NOT TOUCH
trait MovieService {
  def principalsForMovieName(name: String): Source[Principal, _]
  def tvSeriesWithGreatestNumberOfEpisodes(): Source[TvSeries, _]
}

object MovieService {
  final case class TitleId(value: String)     extends AnyRef
  final case class PrincipalId(value: String) extends AnyRef

  final case class Title(
      id: TitleId,
      original: String,
      startYear: Int,
      endYear: Option[Int],
      genres: List[String]
  )
  final case class TitlePrincipal(titleId: TitleId, principalId: PrincipalId)
  final case class PrincipalTmp(
      id: PrincipalId,
      name: String,
      birthYear: Int,
      deathYear: Option[Int],
      profession: List[String]
  )
  final case class Episode(id: TitleId, parentId: TitleId)

  // DO NOT TOUCH
  final case class Principal(name: String, birthYear: Int, deathYear: Option[Int], profession: List[String])
  final case class TvSeries(original: String, startYear: Int, endYear: Option[Int], genres: List[String])
}

class MovieServiceImpl(
    titleSrc: Source[Title, _],
    principalSrc: Source[TitlePrincipal, _],
    nameSrc: Source[PrincipalTmp, _],
    episodeSrc: Source[Episode, _]
) extends MovieService {
  override def principalsForMovieName(name: String): Source[Principal, _] =
    titleSrc
      .filter(_.original == name)
      .map(_.id)
      .flatMapConcat(titleId => principalSrc.filter(_.titleId == titleId).map(_.principalId))
      .flatMapConcat(principalId => nameSrc.filter(_.id == principalId))
      .map(_.into[Principal].transform)

  override def tvSeriesWithGreatestNumberOfEpisodes(): Source[TvSeries, _] =
    episodeSrc
      .fold(Map[TitleId, Int]()) { (map, ep) =>
        val count = map.getOrElse(ep.parentId, 0) + 1
        map + (ep.parentId -> count)
      }
      .map(_.toSeq.sortBy(_._2).reverse.take(10).map(_._1))
      .flatMapConcat(titleIds => titleSrc.filter(title => titleIds.contains(title.id)))
      .map(_.into[TvSeries].transform)
}
