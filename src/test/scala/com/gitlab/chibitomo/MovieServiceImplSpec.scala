package com.gitlab.chibitomo

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import MovieService._
import DataSource._
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.Duration

class MovieServiceImplSpec extends AnyFlatSpec with Matchers with ScalaFutures {
  implicit val actorSystem: ActorSystem = ActorSystem()
  private val titleSrc                  = openTsv[Title]("/title.basics.tsv.gz")
  private val principalSrc              = openTsv[TitlePrincipal]("/title.principals.tsv.gz")
  private val nameSrc                   = openTsv[PrincipalTmp]("/name.basics.tsv.gz")
  private val episodeSrc                = openTsv[Episode]("/title.episode.tsv.gz")

  "tvSeriesWithGreatestNumberOfEpisodes" should "works as intended" in {
    // GIVEN
    val movieService = new MovieServiceImpl(titleSrc, principalSrc, nameSrc, episodeSrc)

    // WHEN
    val resultSrc = movieService
      .tvSeriesWithGreatestNumberOfEpisodes()
      .map { x =>
        println(x)
        x
      }

    // THEN
    val result = resultSrc.runWith(Sink.seq)
    whenReady[Seq[TvSeries], Unit](result, Timeout(Duration.Inf)) { x =>
      println(x)
    }
  }

  "principalsForMovieName" should "works as intended" in {
    // GIVEN
    val movieService = new MovieServiceImpl(titleSrc, principalSrc, nameSrc, episodeSrc)
    val title        = "The Grapes of Wrath"

    // WHEN
    val resultSrc = movieService
      .principalsForMovieName(title)
      .map { x =>
        println(x)
        x
      }

    // THEN
    val result = resultSrc.runWith(Sink.seq)
    whenReady[Seq[Principal], Unit](result, Timeout(Duration.Inf)) { x =>
      println(x)
    }
  }

}
